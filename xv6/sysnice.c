#include "types.h"
#include "defs.h"
#include "param.h"
#include "spinlock.h"
#include "mmu.h"
#include "proc.h"

extern struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  int count;
} ptable;

int sys_nice(void) {
  int inc;
  acquire(&ptable.lock);
  argint( 0, &inc);
  if (proc->level + inc > TWO || proc->level + inc < MTWO)
    return -1;
  proc->level += inc;
  ptable.count -= proc->count;
  int i;
  proc->count = 1;
  for (i = proc->level; i < TWO; i++) {
    proc->count *= 2;
  }
  ptable.count += proc->count;

  // TEST 
  procdump();

  release(&ptable.lock);
  return 0;
}
