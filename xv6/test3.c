#include "types.h"
#include "param.h"
#include "user.h"
#include "stat.h"
#include "mmu.h"

int 
main(int argc, char *argv[])
{
  nice(1);
  nice(-2);
  nice(3);
  // should fail
  /* nice(-2); */
  exit();
}
